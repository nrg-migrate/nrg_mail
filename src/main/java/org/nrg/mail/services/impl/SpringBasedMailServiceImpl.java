/**
 * SpringBasedMailServiceImpl
 * (C) 2011 Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 *
 * Created on Aug 29, 2011 by Rick Herrick <rick.herrick@wustl.edu>
 */
package org.nrg.mail.services.impl;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.nrg.mail.api.MailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service("mailService")
public class SpringBasedMailServiceImpl extends AbstractMailServiceImpl {

	@Override
	public void sendMessage(MailMessage message) throws MessagingException {
		if (StringUtils.isBlank(message.getHtml())
				&& StringUtils.isBlank(message.getOnBehalfOf())
				&& (message.getAttachments() == null || message
						.getAttachments().size() == 0)
				&& (message.getHeaders() == null || message.getHeaders().size() == 0)) {
			if (_log.isDebugEnabled()) {
				_log.debug("Sending email as a simple mail message");
			}
			_sender.send(message.asSimpleMailMessage());
		} else {
			if (_log.isDebugEnabled()) {
				_log.debug("Sending email as a MIME message");
			}
			sendMimeMessage(message.asMimeMessage(getMimeMessage()));
		}
	}

    @Override
    public void sendMessage(MailMessage message, String username, String password) throws MessagingException {
        sendMessage(message);
    }

    /**
	 * Sets the {@link JavaMailSender} object on the service.
	 * 
	 * @param sender
	 *            The sender service instance.
	 */
	public void setJavaMailSender(JavaMailSender sender) {
		_sender = sender;
	}

	/**
	 * Gets a MimeMessage from the mail sender.
	 * 
	 * @return A usable MIME message object.
	 */
	protected MimeMessage getMimeMessage() {
		return _sender.createMimeMessage();
	}

	/**
	 * Sends a MIME message using the mail service. You should create this
	 * message by calling the {@link #getMimeMessage()} method.
	 * 
	 * @param message
	 *            The message to be sent.
	 */
	protected void sendMimeMessage(MimeMessage message) {
		_sender.send(message);
	}

	private static final Logger _log = LoggerFactory.getLogger(SpringBasedMailServiceImpl.class);

	@Inject
	private JavaMailSender _sender;
}
